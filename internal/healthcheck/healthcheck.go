package healthcheck

import (
	"capstone/backend/internal/server"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// import (
// 	"fmt"
// 	"net/http"

// 	"github.com/gin-gonic/gin"
// )

// func (s *Server) HandleHeatlthCheck() gin.HandlerFunc {
// 	//s.db.Collections()
// }

// var dbClient database.Database

//SetupHealthcheckEndpoint creates healthcheck endpoint
func SetupHealthcheckEndpoint(s *server.Server) {
	s.Router.Handle("GET", "/healthcheck", getHealthcheck(s))
}

func getHealthcheck(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		//check if server is alive

		fmt.Println("Evaluating healthcheck")

		serverOnlineError := s.Db.VerifyConnection()

		if serverOnlineError == nil {
			c.String(http.StatusOK, "Online")
		} else {
			c.String(http.StatusGatewayTimeout, "Database Offline")
		}
	}
}
