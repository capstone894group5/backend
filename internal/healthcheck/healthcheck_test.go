package healthcheck

import (
	"bytes"
	"capstone/backend/internal/models"
	"capstone/backend/internal/server"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoMock struct {
	mock.Mock
}

func (m *MongoMock) GetCollection(col string, key string, value string, payload interface{}) error {

	user := models.User{
		ID:       "0",
		Username: "cas6193",
		Password: "$2a$10$4cdJfg1yX8DbefMrvyJZvepH.j/pZMIc.FK0OOwv4yYEy4sXAjB0u",
		Email:    "testemail",
	}

	payload = user

	fmt.Println(payload)

	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetCollectionByID(s string, id primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) InsertCollection(col string, key string, value string, payload interface{}) (string, error) {
	args := m.Called()
	return "", args.Error(0)
}

func (m *MongoMock) UpsertCollection(s string) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAllCollectionsByForeignKey(collectionString string, key string, value primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAll(collectionString string) (*mongo.Cursor, error) {
	args := m.Called()
	return nil, args.Error(0)
}

func (m *MongoMock) GetRandom(collectionString string, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpsertCollectionByID(collectionString string, id primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) VerifyConnection() error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpdateUserInfo(a string, b string, c string, d interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func performRequest(r http.Handler, method, path string, body []byte) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, bytes.NewBuffer(body))
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestGetHealthCheckToReturnStatusCode200GivenDatabaseIsAccessable(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("VerifyConnection").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupHealthcheckEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/healthcheck", nil)

	assert.Equal(t, http.StatusOK, response.Code)
}

func TestGetHealthCheckToReturnStatusCode504GivenDatabaseIsInaccessable(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("VerifyConnection").Return(errors.New("generic error"))

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupHealthcheckEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/healthcheck", nil)

	assert.Equal(t, http.StatusGatewayTimeout, response.Code)
}
