package player

import (
	"capstone/backend/internal/models"
	"capstone/backend/internal/server"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	filePrefix = "/music/"
)

var prodRoot = "cmd/api/music/"
var testRoot = "../../cmd/api/music/"

//FileInfo contains file metadata for a server file
type FileInfo struct {
	Name  string
	IsDir bool
	Mode  os.FileMode
}

//SetupPlayerEndpoint sets up all routers relating to the music player
func SetupPlayerEndpoint(s *server.Server) {
	s.Router.Handle("GET", "/player", HandlePlayerRequest())
	s.Router.Handle("GET", "/music/", HandleMusicFile(s))
	s.Router.Handle("GET", "/music/:artist", HandleMusicFile(s))
	s.Router.Handle("GET", "/music/:artist/:album", HandleMusicFile(s))
	s.Router.Handle("GET", "/music/:artist/:album/:song", HandleMusicFile(s))
}

//HandlePlayerRequest handles a music player request, temporary until frontend is developed
func HandlePlayerRequest() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "player.html", gin.H{})
	}
}

//HandleMusicFile handles requests to serve a new music file
func HandleMusicFile(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("requestURI:", c.Request.URL)
		path := filepath.Join(prodRoot, c.Request.URL.Path[len(filePrefix):])
		fmt.Println(c.Request.URL.Path[len(filePrefix):])
		fmt.Println("path: " + path)
		stat, err := os.Stat(path)
		if err != nil {
			fmt.Println(err)
			c.AbortWithStatusJSON(404, "file not found")
			return
		}
		if stat.IsDir() {
			serveDir(c, path)
			return
		}
		userID := c.Query("id")
		fmt.Println(userID)

		updatePlaycount(c.Request.URL.Path[len(filePrefix):], s)

		updateUserPlaycount(c.Request.URL.Path[len(filePrefix):], userID, s)

		c.Header("Content-Type", "audio/mpeg")
		c.File(path)
	}
}

func serveDir(c *gin.Context, path string) {
	defer func() {
		if _, ok := recover().(error); ok {
			c.AbortWithStatusJSON(401, gin.H{"error": "directory error"})
		}
	}()
	file, err := os.Open(path)
	defer file.Close()
	if err != nil {
		fmt.Print(err)
		return
	}
	files, err := file.Readdir(-1)
	if err != nil {
		fmt.Print(err)
		return
	}
	fileInfos := make([]FileInfo, len(files), len(files))
	for i := range files {
		fileInfos[i].Name = files[i].Name()
		fileInfos[i].IsDir = files[i].IsDir()
		fileInfos[i].Mode = files[i].Mode()
	}
	json.NewEncoder(c.Writer).Encode(&fileInfos)
}

func updatePlaycount(path string, s *server.Server) {
	var song models.Song

	fmt.Println(path)

	path = strings.Replace(path, ".mp3", "", 1)

	s.Db.GetCollection("songs", "path", path, &song)

	song.PlayCount = song.PlayCount + 1

	s.Db.UpsertCollectionByID("songs", song.ID, &song)
}

func updateUserPlaycount(path string, userId string, s *server.Server) {
	var song models.Song
	var album models.Album
	var artist models.Artist
	var user models.User

	var userPlayData models.UserPlayData

	getUserErr := s.Db.GetCollection("users", "username", userId, &user)

	if getUserErr != nil {
		fmt.Println("bad userneame")
	}

	var userIDHex = strings.Replace(user.ID, "ObjectID(\"", "", 1)
	userIDHex = strings.Replace(userIDHex, "\")", "", 1)

	userID, _ := primitive.ObjectIDFromHex(userIDHex)

	path = strings.Replace(path, ".mp3", "", 1)

	s.Db.GetCollection("songs", "path", path, &song)

	s.Db.GetCollectionByID("albums", song.AlbumID, &album)

	s.Db.GetCollectionByID("artists", song.ArtistID, &artist)

	err := s.Db.GetCollectionByID("userData", userID, &userPlayData)

	fmt.Println(err)

	if err != nil {
		var songPlayDataList []models.SongPlayData
		var songPlayData models.SongPlayData

		songPlayData.ID = song.ID
		songPlayData.Album = album.Title
		songPlayData.Artist = artist.Name
		songPlayData.Title = song.Title
		songPlayData.Count = 1

		userPlayData.ID = userID
		userPlayData.PlayData = append(songPlayDataList, songPlayData)

		s.Db.UpsertCollectionByID("userData", userID, &userPlayData)
	} else {
		for index, songData := range userPlayData.PlayData {
			fmt.Println("comparing song IDs")
			fmt.Println(songData.ID)
			fmt.Println(song.ID)
			if songData.ID == song.ID {
				fmt.Println("match found")
				userPlayData.PlayData[index].Count = songData.Count + 1
				s.Db.UpsertCollectionByID("userData", userID, &userPlayData)
				return
			}
		}

		fmt.Println("no match found")

		var songPlayData models.SongPlayData

		songPlayData.ID = song.ID
		songPlayData.Album = album.Title
		songPlayData.Artist = artist.Name
		songPlayData.Title = song.Title
		songPlayData.Count = 1

		userPlayData.PlayData = append(userPlayData.PlayData, songPlayData)
		s.Db.UpsertCollectionByID("userData", userID, &userPlayData)
		return
	}
}
