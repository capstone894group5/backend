package player

import (
	"bytes"
	"capstone/backend/internal/server"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoMock struct {
	mock.Mock
}

func (m *MongoMock) GetCollection(col string, key string, value string, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetCollectionByID(s string, id primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) InsertCollection(col string, key string, value string, payload interface{}) (string, error) {
	args := m.Called()
	return "", args.Error(0)
}

func (m *MongoMock) UpsertCollection(s string) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAllCollectionsByForeignKey(collectionString string, key string, value primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAll(collectionString string) (*mongo.Cursor, error) {
	args := m.Called()
	return nil, args.Error(0)
}

func (m *MongoMock) GetRandom(collectionString string, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpsertCollectionByID(collectionString string, id primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) VerifyConnection() error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpdateUserInfo(a string, b string, c string, d interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func performRequest(r http.Handler, method, path string, body []byte) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, bytes.NewBuffer(body))
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestHandlePlayerRequestWhenCalledReturnsStatusCodeOK(t *testing.T) {
	mockDb := new(MongoMock)

	testEngine := gin.Default()

	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	fmt.Println(path)

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}
	testServer.Router.LoadHTMLFiles("player.html")
	SetupPlayerEndpoint(testServer)

	reponse := performRequest(testEngine, "GET", "/player", nil)

	assert.Equal(t, http.StatusOK, reponse.Code)
}

func TestHandleMusicFileGivenAValidArtistReturnsStatus200(t *testing.T) {
	mockDb := new(MongoMock)

	prodRoot = testRoot

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupPlayerEndpoint(testServer)
	reponse := performRequest(testEngine, "GET", "/music/Foo Fighters", nil)

	assert.Equal(t, http.StatusOK, reponse.Code)
}

func TestHandleMusicFileGivenAnInvalidArtistReturnsStatusNotFound(t *testing.T) {
	mockDb := new(MongoMock)

	testEngine := gin.Default()

	prodRoot = testRoot

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupPlayerEndpoint(testServer)

	reponse := performRequest(testEngine, "GET", "/music/BAD", nil)

	assert.Equal(t, http.StatusNotFound, reponse.Code)
}

func TestHandleMusicFileGivenAValidAlbumReturnsStatus200(t *testing.T) {
	mockDb := new(MongoMock)

	prodRoot = testRoot

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupPlayerEndpoint(testServer)
	reponse := performRequest(testEngine, "GET", "/music/Foo Fighters/Saint Cecilia EP", nil)

	assert.Equal(t, http.StatusOK, reponse.Code)
}

func TestHandleMusicFileGivenAnInvalidAlbumReturnsStatusNotFound(t *testing.T) {
	mockDb := new(MongoMock)

	testEngine := gin.Default()

	prodRoot = testRoot

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupPlayerEndpoint(testServer)

	reponse := performRequest(testEngine, "GET", "/music/Foo Fighters/BAD", nil)

	assert.Equal(t, http.StatusNotFound, reponse.Code)
}

func TestHandleMusicFileGivenAValidSongReturnsStatus200(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(nil)
	mockDb.On("UpsertCollectionByID").Return(nil)
	mockDb.On("GetCollectionByID").Return(nil)

	prodRoot = testRoot

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupPlayerEndpoint(testServer)
	reponse := performRequest(testEngine, "GET", "/music/Foo Fighters/Saint Cecilia EP/01 - Saint Cecilia.mp3", nil)

	assert.Equal(t, http.StatusOK, reponse.Code)
}

func TestHandleMusicFileGivenAnInvalidSongReturnsStatusNotFound(t *testing.T) {
	mockDb := new(MongoMock)

	testEngine := gin.Default()

	prodRoot = testRoot

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupPlayerEndpoint(testServer)

	reponse := performRequest(testEngine, "GET", "/music/Foo Fighters/Saint Cecilia EP/BAD.mp3", nil)

	assert.Equal(t, http.StatusNotFound, reponse.Code)
}
