package library

import (
	"capstone/backend/internal/models"
	"capstone/backend/internal/server"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func SetupLibraryEndpoint(s *server.Server) {
	s.Router.Handle("GET", "/library", GetLibrary(s))
	s.Router.Handle("POST", "/library", UpdateLibrary(s))
}

func GetLibrary(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {

		var library models.UserLibrary

		libraryIDParam := c.Query("id")

		if libraryIDParam == "" {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "no library ID provided"})
			return
		}

		libraryID, _ := primitive.ObjectIDFromHex(libraryIDParam)

		err := s.Db.GetCollectionByID("library", libraryID, &library)

		if err != nil {
			var newLibrary models.UserLibrary
			newLibrary.ID, _ = primitive.ObjectIDFromHex(libraryIDParam)
			s.Db.UpsertCollectionByID("library", newLibrary.ID, newLibrary)
			c.JSON(http.StatusOK, newLibrary)
			return
		}

		c.JSON(http.StatusOK, library)

	}
}

func UpdateLibrary(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		var data map[string]interface{}

		var library models.UserLibrary

		_ = json.NewDecoder(c.Request.Body).Decode(&data)

		libraryID := fmt.Sprintf("%v", data["id"])

		library.Content = fmt.Sprintf("%v", data["content"])

		if libraryID == "" {
			library.ID = primitive.NewObjectID()
		} else {
			library.ID, _ = primitive.ObjectIDFromHex(libraryID)
		}

		fmt.Println(library)

		s.Db.UpsertCollectionByID("library", library.ID, library)

		c.JSON(http.StatusOK, library)

	}
}
