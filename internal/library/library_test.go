package library

import (
	"bytes"
	"capstone/backend/internal/server"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoMock struct {
	mock.Mock
}

func (m *MongoMock) GetCollection(col string, key string, value string, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetCollectionByID(s string, id primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) InsertCollection(col string, key string, value string, payload interface{}) (string, error) {
	args := m.Called()
	return "", args.Error(0)
}

func (m *MongoMock) UpsertCollection(s string) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAllCollectionsByForeignKey(collectionString string, key string, value primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAll(collectionString string) (*mongo.Cursor, error) {
	args := m.Called()
	return nil, args.Error(0)
}

func (m *MongoMock) GetRandom(collectionString string, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpsertCollectionByID(collectionString string, id primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) VerifyConnection() error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpdateUserInfo(a string, b string, c string, d interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func performRequest(r http.Handler, method, path string, body []byte) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, bytes.NewBuffer(body))
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestGetLibraryGivenAValidIDWillReturnStatusCode200(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollectionByID").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupLibraryEndpoint(testServer)

	reponse := performRequest(testEngine, "GET", "/library?id=1", nil)

	assert.Equal(t, http.StatusOK, reponse.Code)
}

func TestGetLibraryGivenAnInvalidIDWillReturnStatusCode400(t *testing.T) {
	mockDb := new(MongoMock)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupLibraryEndpoint(testServer)

	reponse := performRequest(testEngine, "GET", "/library", nil)

	assert.Equal(t, http.StatusBadRequest, reponse.Code)
}

func TestGetLibraryGivenLibraryNotFoundWillReturnStatusCode200(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollectionByID").Return(errors.New("generic error"))
	mockDb.On("UpsertCollectionByID").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupLibraryEndpoint(testServer)

	reponse := performRequest(testEngine, "GET", "/library?id=1", nil)

	assert.Equal(t, http.StatusOK, reponse.Code)
}

func TestGetLibraryGivenAnInvalidUserIDReturnsStatusBadRequest(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollectionByID").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupLibraryEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/library?id=", nil)

	assert.Equal(t, http.StatusBadRequest, response.Code)
}

func TestGetLibraryGivenAUserIDReturnsStatusOK(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollectionByID").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupLibraryEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/library?id=5e92245d0e62f2c5acb039a4", nil)

	assert.Equal(t, http.StatusOK, response.Code)
}

func TestUpdateLibraryGivenAValidLibraryReturnsStatusOK(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("UpsertCollectionByID").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupLibraryEndpoint(testServer)

	testBody := []byte(`{"id":"5e92245d0e62f2c5acb039a4", "content":"test"}`)

	response := performRequest(testEngine, "POST", "/library", testBody)

	assert.Equal(t, http.StatusOK, response.Code)
}

func TestUpdateLibraryGivenAnInvalidLibraryReturnsStatusOK(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("UpsertCollectionByID").Return(errors.New("generic error"))

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupLibraryEndpoint(testServer)

	testBody := []byte(`{"id":"", "content":"test"}`)

	response := performRequest(testEngine, "POST", "/library", testBody)

	assert.Equal(t, http.StatusOK, response.Code)
}
