package music

import (
	"bytes"
	"capstone/backend/internal/server"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var testPayload interface{} = nil

type MongoMock struct {
	mock.Mock
}

func (m *MongoMock) GetCollection(col string, key string, value string, payload interface{}) error {
	payload = testPayload

	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetCollectionByID(s string, id primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) InsertCollection(col string, key string, value string, payload interface{}) (string, error) {
	args := m.Called()
	return "", args.Error(0)
}

func (m *MongoMock) UpsertCollection(s string) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAllCollectionsByForeignKey(collectionString string, key string, value primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAll(collectionString string) (*mongo.Cursor, error) {
	args := m.Called()
	return nil, args.Error(0)
}

func (m *MongoMock) GetRandom(collectionString string, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpsertCollectionByID(collectionString string, id primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) VerifyConnection() error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpdateUserInfo(a string, b string, c string, d interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func performRequest(r http.Handler, method, path string, body []byte) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, bytes.NewBuffer(body))
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestSyncStoreDatabaseGivenDirIsEmptyWillNotCallInsertArtist(t *testing.T) {
	mockDb := new(MongoMock)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SyncSongDatabase("./testmusicempty/", testServer)

	mockDb.AssertNotCalled(t, "GetCollection")
}

func TestSyncStoreDatabaseGivenArtistDirIsEmptyWillNotCallInsertAlbum(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SyncSongDatabase("./testmusicnoalbum/", testServer)

	mockDb.AssertNumberOfCalls(t, "GetCollection", 1)
}

func TestSyncStoreDatabaseGivenSongDirIsEmptyWillNotCallInsertSong(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SyncSongDatabase("./testmusicnosong/", testServer)

	mockDb.AssertNumberOfCalls(t, "GetCollection", 2)
}

func TestSyncStoreDatabaseGivenArtistDoesNotExistWillInsert(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(errors.New("generic error"))

	mockDb.On("InsertCollection").Return(nil, nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SyncSongDatabase("./testmusicnoalbum/", testServer)

	mockDb.AssertNumberOfCalls(t, "InsertCollection", 1)
}

func TestSyncStoreDatabaseGivenArtistExistsWillNotInsert(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(nil)

	mockDb.On("InsertCollection").Return(nil, nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SyncSongDatabase("./testmusicnoalbum/", testServer)

	mockDb.AssertNotCalled(t, "InsertCollection")
}

func TestSyncStoreDatabaseGivenAlbumDoesNotExistWillInsert(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(errors.New("generic error"))

	mockDb.On("InsertCollection").Return(nil, "123456789")

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SyncSongDatabase("./testmusicnosong/", testServer)

	mockDb.AssertNumberOfCalls(t, "InsertCollection", 2)
}

func TestSyncStoreDatabaseGivenAlbumExistsWillNotInsert(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(nil)

	mockDb.On("InsertCollection").Return(nil, "123456789")

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SyncSongDatabase("./testmusicnosong/", testServer)

	mockDb.AssertNotCalled(t, "InsertCollection")
}

func TestSyncStoreDatabaseGivenSongDoesNotExistWillInsert(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(errors.New("generic error"))

	mockDb.On("InsertCollection").Return(nil, "123456789")

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SyncSongDatabase("./testmusic/", testServer)

	mockDb.AssertNumberOfCalls(t, "InsertCollection", 3)
}

func TestSyncStoreDatabaseGivenSongExistsWillNotInsert(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(nil)

	mockDb.On("InsertCollection").Return(nil, "123456789")

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SyncSongDatabase("./testmusic/", testServer)

	mockDb.AssertNotCalled(t, "InsertCollection")
}
