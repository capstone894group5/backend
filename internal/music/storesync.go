package music

import (
	"capstone/backend/internal/models"
	"capstone/backend/internal/server"
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Grab all folders in 1st dir, search if exists, make db entry if does not exist
func SyncSongDatabase(musicDir string, s *server.Server) error {
	baseFiles, err := ioutil.ReadDir(musicDir)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range baseFiles {
		if f.IsDir() {
			artistName := f.Name()
			artistId, _ := insertArtist(artistName, "/"+artistName, s)
			parseAlbums(musicDir+artistName+"/", artistId, s)

		}
	}

	return err
}

func parseAlbums(artistPath string, artistId primitive.ObjectID, s *server.Server) error {
	albumDirs, err := ioutil.ReadDir(artistPath)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range albumDirs {
		if f.IsDir() {
			albumName := f.Name()
			albumId, _ := insertAlbum(albumName, artistId, artistPath+albumName, s)
			parseSongs(artistPath+albumName+"/", albumId, artistId, s)

		}
	}

	return err
}

func parseSongs(songPath string, albumId primitive.ObjectID, artistId primitive.ObjectID, s *server.Server) error {
	albumDirs, err := ioutil.ReadDir(songPath)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range albumDirs {
		songName := f.Name()
		songName = strings.Replace(songName, ".mp3", "", 1)
		insertSong(songName, albumId, artistId, songPath+songName, s)
	}

	return err
}

func insertArtist(artistName string, path string, s *server.Server) (primitive.ObjectID, error) {
	var artist models.Artist
	err := s.Db.GetCollection("artists", "name", artistName, &artist)

	if err != nil {
		fmt.Println("artist " + artistName + " not found inserting")
		artist = models.Artist{
			Name: artistName,
			Path: path,
		}
		idstr, insertErr := s.Db.InsertCollection("artists", "name", artistName, &artist)
		idstr = strings.Replace(idstr, "ObjectID(\"", "", 1)
		idstr = strings.Replace(idstr, "\")", "", 1)
		id, _ := primitive.ObjectIDFromHex(idstr)
		return id, insertErr
	} else {
		return artist.ID, nil
		//return artist id
	}
}

func insertAlbum(albumName string, artistId primitive.ObjectID, path string, s *server.Server) (primitive.ObjectID, error) {
	var album models.Album
	err := s.Db.GetCollection("albums", "title", albumName, &album)

	if err != nil {
		fmt.Println("album " + albumName + "not found inserting")
		album = models.Album{
			Title:  albumName,
			Artist: artistId,
			Path:   strings.Replace(path, "cmd/api/music/", "", 1),
		}
		idstr, insertErr := s.Db.InsertCollection("albums", "name", albumName, &album)
		idstr = strings.Replace(idstr, "ObjectID(\"", "", 1)
		idstr = strings.Replace(idstr, "\")", "", 1)
		id, _ := primitive.ObjectIDFromHex(idstr)
		return id, insertErr
	} else {
		return album.ID, nil
		//return artist id
	}
}

func insertSong(songName string, albumId primitive.ObjectID, artistId primitive.ObjectID, path string, s *server.Server) (primitive.ObjectID, error) {
	var song models.Song
	err := s.Db.GetCollection("songs", "title", songName, &song)

	if err != nil {
		fmt.Println("song " + songName + " not found inserting")
		song = models.Song{
			Title:     songName,
			ArtistID:  artistId,
			AlbumID:   albumId,
			Path:      strings.Replace(path, "cmd/api/music/", "", 1),
			PlayCount: 0,
		}
		idstr, insertErr := s.Db.InsertCollection("songs", "title", songName, &song)
		idstr = strings.Replace(idstr, "ObjectID(\"", "", 1)
		idstr = strings.Replace(idstr, "\")", "", 1)
		id, _ := primitive.ObjectIDFromHex(idstr)
		return id, insertErr
	} else {
		return song.ID, nil
	}
}
