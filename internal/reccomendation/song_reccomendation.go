package reccomendation

import (
	"capstone/backend/internal/models"
	"capstone/backend/internal/server"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"sort"
	"strings"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

//Takes top 5 songs from users with current song in top. Scoring is weighted by what %

//top 10% = 100% value
// down 10% value based on where the song ranks in their top
// From there all songs are added and weighted similarly
// Top 5 song reccomendations are stored in the database

func SongReccomendations(s *server.Server) {

	fmt.Println("Creating Song Reccomendations")

	var songRanking map[string]float32

	//get song cursor
	cursor, err := s.Db.GetAll("songs")

	if err != nil {
		log.Fatal(err)
	}

	//for each song
	for cursor.Next(context.TODO()) {
		songRanking = make(map[string]float32)

		//decode song at cursor pointer
		var song models.Song
		if err = cursor.Decode(&song); err != nil {
			log.Fatal(err)
		}

		//Grab all users with song in their played list
		var usercursor, usercursorerror = s.Db.GetAll("userData")

		if usercursorerror != nil {
			log.Fatal(err)
		}

		for usercursor.Next(context.TODO()) {
			var userData models.UserPlayData

			var userScoring = make(map[primitive.ObjectID]int)

			var totalPlays int

			var userweight float32

			if err = usercursor.Decode(&userData); err != nil {
				log.Fatal(err)
			}
			for _, userSongData := range userData.PlayData {
				userScoring[userSongData.ID] = userSongData.Count

				totalPlays = totalPlays + userSongData.Count
			}

			if usersongplays, ok := userScoring[song.ID]; ok {
				userweight = float32(usersongplays) / float32(totalPlays)
			} else {
				continue
			}

			for index, usertopsongplays := range userScoring {
				var indexString string
				indexString = index.Hex()
				if index != song.ID {
					if _, ok := songRanking[indexString]; ok {
						songRanking[indexString] = songRanking[indexString] + ((float32(usertopsongplays) / float32(totalPlays)) * userweight)
					} else {
						songRanking[indexString] = ((float32(usertopsongplays) / float32(totalPlays)) * userweight)
					}
				}

			}

			var songReccomendations models.SongReccomendation
			songReccomendations.ID = song.ID
			songReccomendations.ReccomendedSongs = songRanking

			s.Db.UpsertCollectionByID("reccomendation", song.ID, songReccomendations)
		}

	}

	fmt.Println("Song Reccomendations Complete")
}

func CreateReccomenededPlaylists(s *server.Server) {

	fmt.Println("Generating Playlists")

	userCursor, err := s.Db.GetAll("users")

	if err != nil {
		fmt.Println(err)
		return
	}
	//for each user
	for userCursor.Next(context.TODO()) {

		var user models.User
		var userPlayData models.UserPlayData

		if err = userCursor.Decode(&user); err != nil {
			log.Fatal(err)
		}

		var userIDHex = strings.Replace(user.ID, "ObjectID(\"", "", 1)
		userIDHex = strings.Replace(userIDHex, "\")", "", 1)

		userID, err := primitive.ObjectIDFromHex(userIDHex)

		if err != nil {
			fmt.Println(err)
			continue
		}

		// grab top 5 song ids
		err = s.Db.GetCollectionByID("userData", userID, &userPlayData)

		if err != nil {
			fmt.Println(err)
			continue
		}

		sort.Slice(userPlayData.PlayData[:], func(i, j int) bool {
			return userPlayData.PlayData[i].Count > userPlayData.PlayData[j].Count
		})

		//take song list and move it down to 5 if 5 do not exist pull randoms and add

		var userTopSongs []models.SongPlayData

		userTopSongs = userPlayData.PlayData

		if len(userTopSongs) > 5 {
			userTopSongs = userTopSongs[:5]
		} else {
			for len(userTopSongs) < 5 {
				var randomSong models.Song
				s.Db.GetRandom("songs", &randomSong)

				var addedSongData models.SongPlayData

				addedSongData.ID = randomSong.ID

				userTopSongs = append(userTopSongs, addedSongData)
			}
		}

		reccomendedPlaylist := generatePlaylist(s, userTopSongs)

		var userLibrary models.UserLibrary
		var libraryContent models.UserLibraryContent

		s.Db.GetCollectionByID("library", user.Library, &userLibrary)

		json.Unmarshal([]byte(userLibrary.Content), &libraryContent)
		var recPlaylistExists bool = false
		for index, playlist := range libraryContent.Playlists {
			recPlaylistExists = false
			if playlist.Title == "Recommended" {
				recPlaylistExists = true
				libraryContent.Playlists[index] = reccomendedPlaylist
				newContent, _ := json.Marshal(libraryContent)
				userLibrary.Content = string(newContent)
				userLibrary.ID = user.Library
				s.Db.UpsertCollectionByID("library", user.Library, userLibrary)
				continue
			}
		}

		if recPlaylistExists == false {
			libraryContent.Playlists = append(libraryContent.Playlists, reccomendedPlaylist)
			newContent, _ := json.Marshal(libraryContent)
			userLibrary.Content = string(newContent)
			userLibrary.ID = user.Library
			s.Db.UpsertCollectionByID("library", user.Library, userLibrary)
		}

	}

	fmt.Println("Playlist Generation Complete")
}

func generatePlaylist(s *server.Server, userTopSongs []models.SongPlayData) models.ReccomendedPlaylist {
	var reccomendedPlaylist models.ReccomendedPlaylist

	for _, topSong := range userTopSongs {
		var songID = topSong.ID
		for i := 1; i <= 5; i++ {
			var songRecs models.SongReccomendation
			err := s.Db.GetCollectionByID("reccomendation", songID, &songRecs)
			if err != nil {
				fmt.Println(err)
			}
			var totalSongWeight float32 = 0
			var reccomendedSongArray []ReccomendedSongs
			for index, songWeight := range songRecs.ReccomendedSongs {
				var song ReccomendedSongs
				songID, _ := primitive.ObjectIDFromHex(index)
				song.ID = songID
				song.randRange = totalSongWeight + songWeight
				reccomendedSongArray = append(reccomendedSongArray, song)
				totalSongWeight = totalSongWeight + songWeight
			}
			r := rand.Float32() * (totalSongWeight)
			var songResult primitive.ObjectID
			for _, song := range reccomendedSongArray {
				if song.randRange > r {
					songResult = song.ID
					break
				}
			}

			songID = songResult

			var playlistSong models.PlaylistSong

			var songData models.Song
			var albumData models.Album
			var artistData models.Artist

			s.Db.GetCollectionByID("songs", songResult, &songData)
			s.Db.GetCollectionByID("albums", songData.AlbumID, &albumData)
			s.Db.GetCollectionByID("artists", songData.ArtistID, &artistData)

			playlistSong.Title = songData.Title
			playlistSong.StreamURL = "https://lion-music-server.herokuapp.com/music/" + songData.Path + ".mp3"
			playlistSong.ArtistName = artistData.Name
			playlistSong.AlbumTitle = albumData.Title
			playlistSong.DisplayName = songData.Title

			reccomendedPlaylist.Songs = append(reccomendedPlaylist.Songs, playlistSong)
		}
	}

	reccomendedPlaylist.Title = "Recommended"
	return reccomendedPlaylist
}

type ReccomendedSongs struct {
	ID        primitive.ObjectID
	randRange float32
}
