package auth

import (
	"capstone/backend/internal/database"
	"capstone/backend/internal/models"
	"capstone/backend/internal/server"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"golang.org/x/crypto/bcrypt"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/gin-gonic/gin"
)

//SetupAuthEndpoint sets up all routers for the authorization endpoint
func SetupAuthEndpoint(s *server.Server) {
	s.Router.Handle("POST", "/register", HandleAuthRegister(s))
	s.Router.Handle("GET", "/", HandleAuthIndex(s))
	s.Router.Handle("POST", "/login", HandleAuthLoginRequest(s))
	s.Router.Handle("POST", "/profile", HandleAuthProfile(s))
	s.Router.Handle("POST", "/passwordChange", HandleAuthPasswordChange(s))
	s.Router.Handle("POST", "/emailChange", HandleAuthEmailChange(s))
	s.Router.Handle("POST", "/authenticatedProfile", TokenAuthentication(HandleAuthProfile(s)))
}

//HandleAuthIndex handles redirects to login page
func HandleAuthIndex(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.String(200, "Lion server is running!")
	}
}

//HandleAuthRegister handles new user registration request
func HandleAuthRegister(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		var data map[string]interface{}
		var user models.User

		//parse incoming post
		_ = json.NewDecoder(c.Request.Body).Decode(&data)

		//Create user object
		id := primitive.NewObjectID().String()
		passwordHash, _ := bcrypt.GenerateFromPassword([]byte(data["password"].(string)), bcrypt.DefaultCost)

		user = models.User{
			ID:       id,
			Username: data["username"].(string),
			Password: string(passwordHash),
			Email:    data["email"].(string),
			Library:  primitive.NewObjectID(),
		}

		//make insert attempt on database
		_, insertErr := s.Db.InsertCollection("users", "username", data["username"].(string), &user)
		if insertErr != nil {
			switch insertErr.(type) {
			case database.KeyExistsError:
				c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "Username already exists"})
			default:
				c.AbortWithStatusJSON(500, gin.H{"error": "Internal server error"})
			}
			return
		}

		c.JSON(200, gin.H{
			"code":    http.StatusOK,
			"message": "registration success",
		})

		c.Next()
	}
}

//HandleAuthLoginRequest handles login requests
func HandleAuthLoginRequest(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		var data map[string]interface{}
		var user models.User

		_ = json.NewDecoder(c.Request.Body).Decode(&data)

		err := s.Db.GetCollection("users", "username", data["username"].(string), &user)

		fmt.Println(user)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "user not found"})
			return
		}

		fmt.Println(user)

		//pword comparison
		err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(data["password"].(string)))
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "incorrect password"})
			return
		}

		c.JSON(200, gin.H{
			"code":    http.StatusOK,
			"message": "login success",
		})
	}
}

func HandleAuthPasswordChange(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		var data map[string]interface{}
		_ = json.NewDecoder(c.Request.Body).Decode(&data)

		var user models.User
		err := s.Db.GetCollection("users", "username", data["username"].(string), &user)

		if err != nil {
			c.JSON(401, gin.H{
				"code":    http.StatusUnauthorized,
				"message": "user not found",
			})
			return
		}

		newPasswordHash, _ := bcrypt.GenerateFromPassword([]byte(data["password"].(string)), bcrypt.DefaultCost)

		user.Password = string(newPasswordHash)
		err = s.Db.UpdateUserInfo("users", "username", data["username"].(string), &user)

		if err != nil {
			fmt.Println("password update error")
			fmt.Println(err)
			return
		}

		c.JSON(200, gin.H{
			"code":    http.StatusOK,
			"message": "password changed successfully",
		})
	}
}

func HandleAuthEmailChange(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		var data map[string]interface{}
		_ = json.NewDecoder(c.Request.Body).Decode(&data)

		var user models.User
		err := s.Db.GetCollection("users", "username", data["username"].(string), &user)

		if err != nil {
			c.JSON(401, gin.H{
				"code":    http.StatusUnauthorized,
				"message": "user not found",
			})
			return
		}

		newEmail := data["email"].(string)

		user.Email = string(newEmail)
		err = s.Db.UpdateUserInfo("users", "username", data["username"].(string), &user)

		if err != nil {
			fmt.Println("email update error")
			fmt.Println(err)
			return
		}

		c.JSON(200, gin.H{
			"code":    http.StatusOK,
			"message": "email changed successfully",
		})
	}
}

//HandleAuthProfile handles user profile requests
func HandleAuthProfile(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		var data map[string]interface{}
		var user models.User
		_ = json.NewDecoder(c.Request.Body).Decode(&data)

		err := s.Db.GetCollection("users", "username", data["username"].(string), &user)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "user not found"})
			return
		}

		c.Status(http.StatusOK)
		user.Password = "********"
		json.NewEncoder(c.Writer).Encode(user)

		c.Next()
	}
}

//TokenAuthentication prevents unauthorized requests from being handled
func TokenAuthentication(h gin.HandlerFunc) gin.HandlerFunc {
	requiredToken := "token123"
	return func(c *gin.Context) {
		authorizationHeader := c.Request.Header.Get("authorization")
		if authorizationHeader != "" {
			token := strings.Split(authorizationHeader, " ")
			if len(token) == 2 {
				if token[1] == "" {
					c.AbortWithStatusJSON(401, gin.H{"error": "no token in authorization header"})
					return
				}
				if token[1] != requiredToken {
					c.AbortWithStatusJSON(401, gin.H{"error": "invalid token in authorization header"})
					return
				}
			}
		} else {
			c.AbortWithStatusJSON(401, gin.H{"error": "no authorization header token"})
			return
		}
		h(c)
	}
}
