package auth

import (
	"bytes"
	"capstone/backend/internal/database"
	"capstone/backend/internal/models"
	"capstone/backend/internal/server"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MongoMock struct {
	mock.Mock
}

func (m *MongoMock) GetCollection(col string, key string, value string, payload interface{}) error {

	user := models.User{
		ID:       "0",
		Username: "cas6193",
		Password: "$2a$10$4cdJfg1yX8DbefMrvyJZvepH.j/pZMIc.FK0OOwv4yYEy4sXAjB0u",
		Email:    "testemail",
	}

	payload = user

	fmt.Println(payload)

	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetCollectionByID(s string, id primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) InsertCollection(col string, key string, value string, payload interface{}) (string, error) {
	args := m.Called()
	return "", args.Error(0)
}

func (m *MongoMock) UpsertCollection(s string) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAllCollectionsByForeignKey(collectionString string, key string, value primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAll(collectionString string) (*mongo.Cursor, error) {
	args := m.Called()
	return nil, args.Error(0)
}

func (m *MongoMock) GetRandom(collectionString string, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpsertCollectionByID(collectionString string, id primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) VerifyConnection() error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpdateUserInfo(a string, b string, c string, d interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func performRequest(r http.Handler, method, path string, body []byte) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, bytes.NewBuffer(body))
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestHandleAuthIndexGivenCalledReturnsStatusCode200(t *testing.T) {
	mockDb := new(MongoMock)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAuthEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/", nil)

	assert.Equal(t, http.StatusOK, response.Code)
}

func TestHandleAuthRegisterGivenUserDoesNotExistAndDataWrittenToMongoReturnsStatusOK(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("InsertCollection").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAuthEndpoint(testServer)

	testBody := []byte(`{"username":"test", "password":"test","email":"testemail"}`)

	response := performRequest(testEngine, "POST", "/register", testBody)

	assert.Equal(t, http.StatusOK, response.Code)
}

func TestHandleAuthRegisterGivenUserAlreadyExistsReturnsStatusBadRequest(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("InsertCollection").Return(*new(database.KeyExistsError))

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAuthEndpoint(testServer)

	testBody := []byte(`{"username":"test", "password":"test","email":"testemail"}`)

	response := performRequest(testEngine, "POST", "/register", testBody)

	assert.Equal(t, http.StatusBadRequest, response.Code)
}

func TestHandleAuthRegisterGivenUserDoesNotExistAndDataWriteFailsReturnsStatus500(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("InsertCollection").Return(errors.New("generic error"))

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAuthEndpoint(testServer)

	testBody := []byte(`{"username":"test", "password":"test","email":"testemail"}`)

	response := performRequest(testEngine, "POST", "/register", testBody)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
}

func TestHandleAuthLoginRequestGivenUserDoesNotExistsReturnsStatusCodeUnauthorized(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(errors.New("generic error"))

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAuthEndpoint(testServer)

	testBody := []byte(`{"username":"cas6193", "password":"password"}`)

	response := performRequest(testEngine, "POST", "/login", testBody)

	assert.Equal(t, http.StatusUnauthorized, response.Code)
}

func TestHandleAuthLoginRequestGivenUserDoesExistAndPasswordDoesNotMatchReturnsStatusCodeUnauthorized(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAuthEndpoint(testServer)

	testBody := []byte(`{"username":"cas6193", "password":""}`)

	response := performRequest(testEngine, "POST", "/login", testBody)

	assert.Equal(t, http.StatusUnauthorized, response.Code)
}

func TestHandleAuthProfileGivenUserDoesNotExistReturnsStatusCodeBadRequest(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(errors.New("generic error"))

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAuthEndpoint(testServer)

	testBody := []byte(`{"username":"cas6193"}`)

	response := performRequest(testEngine, "POST", "/profile", testBody)

	assert.Equal(t, http.StatusBadRequest, response.Code)
}

func TestHandleAuthProfileGivenUserDoesExistReturnsStatusCodeStatusOK(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAuthEndpoint(testServer)

	testBody := []byte(`{"username":"cas6193"}`)

	response := performRequest(testEngine, "POST", "/profile", testBody)

	assert.Equal(t, http.StatusOK, response.Code)
}

func TestTokenAuthenticationGivenNoAuthTokenIsAddedReturnsStatusCodeUnauthorized(t *testing.T) {
	mockDb := new(MongoMock)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAuthEndpoint(testServer)

	req, _ := http.NewRequest("POST", "/authenticatedProfile", nil)

	w := httptest.NewRecorder()

	testEngine.ServeHTTP(w, req)

	assert.Equal(t, http.StatusUnauthorized, w.Code)
}

func TestTokenAuthenticationGivenBadTokenIsAddedReturnsStatusCodeUnauthorized(t *testing.T) {
	mockDb := new(MongoMock)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAuthEndpoint(testServer)

	req, _ := http.NewRequest("POST", "/authenticatedProfile", nil)
	req.Header.Add("Authorization", "Basic BADTOKEN")

	w := httptest.NewRecorder()

	testEngine.ServeHTTP(w, req)

	assert.Equal(t, http.StatusUnauthorized, w.Code)
}

func TestTokenAuthenticationGivenAuthTokenIsEmptyReturnsStatusCodeUnauthorized(t *testing.T) {
	mockDb := new(MongoMock)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAuthEndpoint(testServer)

	req, _ := http.NewRequest("POST", "/authenticatedProfile", nil)
	req.Header.Add("Authorization", "")

	w := httptest.NewRecorder()

	testEngine.ServeHTTP(w, req)

	assert.Equal(t, http.StatusUnauthorized, w.Code)
}
