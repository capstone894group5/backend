package playlist

import (
	"bytes"
	"capstone/backend/internal/models"
	"capstone/backend/internal/server"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoMock struct {
	mock.Mock
}

func (m *MongoMock) GetCollection(col string, key string, value string, payload interface{}) error {

	playlist := models.Playlist{
		ID:      primitive.NewObjectID(),
		UserID:  primitive.NewObjectID(),
		Content: "test",
	}

	payload = playlist

	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetCollectionByID(col string, id primitive.ObjectID, payload interface{}) error {

	playlist := models.Playlist{
		ID:      primitive.NewObjectID(),
		UserID:  primitive.NewObjectID(),
		Content: "test",
	}

	payload = playlist

	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) InsertCollection(col string, key string, value string, payload interface{}) (string, error) {
	args := m.Called()
	return "", args.Error(0)
}

func (m *MongoMock) UpsertCollection(s string) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAllCollectionsByForeignKey(collectionString string, key string, value primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAll(collectionString string) (*mongo.Cursor, error) {
	args := m.Called()
	return nil, args.Error(0)
}

func (m *MongoMock) GetRandom(collectionString string, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpsertCollectionByID(collectionString string, id primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) VerifyConnection() error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpdateUserInfo(a string, b string, c string, d interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func performRequest(r http.Handler, method, path string, body []byte) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, bytes.NewBuffer(body))
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestGetPlaylistGivenAnInvalidPlaylistReturnsStatusBadRequest(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollectionByID").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupPlaylistEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/playlist?id=", nil)

	assert.Equal(t, http.StatusBadRequest, response.Code)
}

func TestGetPlaylistGivenGetCollectionThrowsErrorReturnsInternalServerError(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollectionByID").Return(errors.New("generic error"))
	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupPlaylistEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/playlist?id=BAD", nil)

	assert.Equal(t, http.StatusNotFound, response.Code)
}

func TestGetPlaylistGivenAValidPlaylistReturnsAValidPlaylistObject(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollectionByID").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupPlaylistEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/playlist?id=GOOD", nil)

	assert.Equal(t, http.StatusOK, response.Code)
}

func TestPostPlaylistGivenAValidEmptyPlaylistReturnsAStatusCode200(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("UpsertCollectionByID").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupPlaylistEndpoint(testServer)

	testBody := []byte(`{"id":"5e92245d0e62f2c5acb039a6", "user_id":"5e92245d0e62f2c5acb039a6", "content":""}`)

	response := performRequest(testEngine, "POST", "/playlist", testBody)

	assert.Equal(t, http.StatusOK, response.Code)
}

func TestPostPlaylistGivenANilValueReturnsAStatusCode400(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("UpsertCollectionByID").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupPlaylistEndpoint(testServer)

	response := performRequest(testEngine, "POST", "/playlist", nil)

	assert.Equal(t, http.StatusBadRequest, response.Code)
}

func TestGetUserPlaylistsGivenValidIDReturnsStatusCode200(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetAllCollectionsByForeignKey").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupPlaylistEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/userplaylists?id=5e92245d0e62f2c5acb039a6", nil)

	assert.Equal(t, http.StatusOK, response.Code)
}

func TestGetUserPlaylistsGivenInvalidIDReturnsBadRequest(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetAllCollectionsByForeignKey").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupPlaylistEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/userplaylists?id=", nil)

	assert.Equal(t, http.StatusBadRequest, response.Code)
}
