package playlist

import (
	"capstone/backend/internal/models"
	"capstone/backend/internal/server"
	"encoding/json"
	"fmt"
	"net/http"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/gin-gonic/gin"
)

//SetupPlaylistEndpoint will setup each endpoint used to manipulate playlists
func SetupPlaylistEndpoint(s *server.Server) {

	s.Router.Handle("GET", "/playlist", getPlaylist(s))
	s.Router.Handle("GET", "/userplaylists", GetUserPlaylists(s))
	s.Router.Handle("POST", "/playlist", UpdatePlaylist(s))

}

func getPlaylist(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {

		var playlist models.Playlist

		playlistIDParam := c.Query("id")

		if playlistIDParam == "" {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "no playlist Id provided"})
			return
		}

		playlistID, _ := primitive.ObjectIDFromHex(playlistIDParam)

		fmt.Println(playlistID)

		err := s.Db.GetCollectionByID("playlists", playlistID, &playlist)

		fmt.Println("get col error")
		fmt.Println(err)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "playlist not found"})
			return
		}
		c.JSON(http.StatusOK, playlist)
	}
}

func GetUserPlaylists(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		var playlists []models.Playlist

		userIDParam := c.Query("id")

		if userIDParam == "" {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "no user Id provided"})
			return
		}

		userID, _ := primitive.ObjectIDFromHex(userIDParam)

		fmt.Println(userID)

		err := s.Db.GetAllCollectionsByForeignKey("playlists", "user_id", userID, &playlists)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "user not found"})
			return
		}

		var userplaylists models.UserPlaylists

		for _, element := range playlists {
			userplaylists.IDs = append(userplaylists.IDs, element.ID)
		}

		c.JSON(http.StatusOK, userplaylists)

	}
}

func UpdatePlaylist(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		var data map[string]interface{}

		var playlist models.Playlist

		_ = json.NewDecoder(c.Request.Body).Decode(&data)

		playlistID := fmt.Sprintf("%v", data["id"])

		userID := fmt.Sprintf("%v", data["user_id"])

		userIDObj, userErr := primitive.ObjectIDFromHex(userID)

		playlist.UserID = userIDObj

		if userErr != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "bad or no userID"})
		}

		playlist.Content = fmt.Sprintf("%v", data["content"])

		if playlistID == "" {
			playlist.ID = primitive.NewObjectID()
		} else {
			playlist.ID, _ = primitive.ObjectIDFromHex(playlistID)
		}

		fmt.Println(playlist)

		s.Db.UpsertCollectionByID("playlists", playlist.ID, playlist)

		c.JSON(http.StatusOK, playlist)

	}
}
