package database

import (
	"capstone/backend/internal/models"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

const testConnection = "mongodb://localhost:27017"
const badConnection = "bad"

type mongoConnectionMock struct {
	mock.Mock
}

type mongoClientMock struct {
	mock.Mock
}

type mongoCollectionMock struct {
	mock.Mock
}

func (m *mongoConnectionMock) VerifyConnection() error {
	args := m.Called()
	return args.Error(0)
}

func (m *mongoCollectionMock) FindOne() error {
	args := m.Called()
	return args.Error(0)
}

func (m *mongoCollectionMock) InsertOne() error {
	args := m.Called()
	return args.Error(0)
}

func TestSetupDatabaseReturnsPingError(t *testing.T) {
	connection := new(mongoConnectionMock)
	connection.On("VerifyConnection").Return(errors.New("ping error"))
	db, err := SetupDatabase()
	assert.Nil(t, err)
	assert.NotNil(t, db)
}

func TestInsertDocumentReturnsKeyExistsError(t *testing.T) {
	collection := new(mongoCollectionMock)
	collection.On("FindOne").Return(nil)
	testConnection, err := SetupDatabase()
	var user models.User
	_, err = testConnection.InsertCollection("users", "username", "newUser", &user)
	assert.NotNil(t, err)
}

func TestInsertDocumentReturnsInsertError(t *testing.T) {
	collection := new(mongoCollectionMock)
	collection.On("InsertOne").Return(errors.New("insert error"))
	testConnection, err := SetupDatabase()
	var user models.User
	_, err = testConnection.InsertCollection("users", "unusedName", "pass", &user)
	assert.NotNil(t, err)
}

func TestGetDocumentReturnsError(t *testing.T) {
	collection := new(mongoCollectionMock)
	collection.On("FindOne").Return(nil)

	testConnection, err := SetupDatabase()
	var user models.User
	err = testConnection.GetCollection("users", "key", "value", &user)
	assert.NotNil(t, err)
}

func TestGetNewMongoClientReturnsClient(t *testing.T) {
	client, err := getNewMongoClient(testConnection)
	assert.NotNil(t, client)
	assert.Nil(t, err)
}

func TestGetNewMongoClientReturnsError(t *testing.T) {
	client, err := getNewMongoClient(badConnection)
	assert.NotNil(t, err)
	assert.Nil(t, client)
}
