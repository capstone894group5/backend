package database

import "fmt"

//KeyExistsError thrown if a key exists in the database
type KeyExistsError struct {
	key string
}

func (e KeyExistsError) Error() string {
	return fmt.Sprintf("Key %s already exists", e.key)
}
