package database

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const atlasConnection = "mongodb+srv://backend:KlC3spKwAfIQs0Fx@capstonecluster-gwv8z.mongodb.net/test?retryWrites=true&w=majority"
const localConnection = "mongodb://localhost:27017"
const databaseName = "capstoneDB"

type MongoConnection struct {
	Client   *mongo.Client
	Database *mongo.Database
}

//SetupDatabase gets the database connection running
func SetupDatabase() (MongoWrapper, error) {
	mongoConnection, err := getNewMongoClient(atlasConnection)
	if err != nil {
		return nil, err
	}

	pingErr := mongoConnection.VerifyConnection()
	if pingErr != nil {
		return nil, pingErr
	}

	mongoConnection.Database = mongoConnection.Client.Database(databaseName)
	return mongoConnection, nil
}

//GetCollection
func (m *MongoConnection) GetCollection(collectionString string, key string, value string, payload interface{}) error {
	var collection = m.Database.Collection(collectionString)

	//docCount, countErr := collection.EstimatedDocumentCount(context.TODO())

	err := collection.FindOne(context.TODO(), bson.M{key: value}).Decode(payload)

	return err
}

//GetCollectionByID will grab an object by its primitive id
func (m *MongoConnection) GetCollectionByID(collectionString string, id primitive.ObjectID, payload interface{}) error {
	var collection = m.Database.Collection(collectionString)

	err := collection.FindOne(context.TODO(), bson.M{"_id": id}).Decode(payload)

	return err
}

func (m *MongoConnection) GetAllCollectionsByForeignKey(collectionString string, key string, value primitive.ObjectID, payload interface{}) error {
	var collection = m.Database.Collection(collectionString)

	cursor, err := collection.Find(context.TODO(), bson.M{key: value})

	cursor.All(context.TODO(), payload)

	return err
}

func (m *MongoConnection) GetAll(collectionString string) (*mongo.Cursor, error) {
	var collection = m.Database.Collection(collectionString)

	cursor, err := collection.Find(context.TODO(), bson.M{})

	return cursor, err
}

func (m *MongoConnection) GetRandom(collectionString string, payload interface{}) error {
	var collection = m.Database.Collection(collectionString)

	pipeline := []bson.M{
		bson.M{"$sample": bson.M{
			"size": 1}}}

	cursor, getError := collection.Aggregate(context.TODO(), pipeline)
	if getError != nil {
		fmt.Println("errorr during aggregation")
		fmt.Println(getError)
	}

	var decodeErr error

	for cursor.Next(context.TODO()) {
		decodeErr = cursor.Decode(payload)
	}

	if decodeErr != nil {
		fmt.Println(decodeErr)
	}

	return decodeErr
}

//InsertCollection
func (m *MongoConnection) InsertCollection(collectionString string, key string, value string, payload interface{}) (string, error) {

	var collection = m.Database.Collection(collectionString)

	tempPayload := payload

	item := collection.FindOne(context.TODO(), bson.M{key: value})

	getErr := item.Decode(&tempPayload)

	if getErr == nil {
		return "", KeyExistsError{
			key: key,
		}
	}

	result, InsertErr := collection.InsertOne(context.TODO(), payload)
	insertId := fmt.Sprintf("%v", result.InsertedID)

	return insertId, InsertErr

}

//UpsertCollection
func (m *MongoConnection) UpsertCollectionByID(collectionString string, id primitive.ObjectID, payload interface{}) error {
	var collection = m.Database.Collection(collectionString)
	result := collection.FindOne(context.TODO(), bson.M{"_id": id})

	tempPayload := payload

	decodeErr := result.Decode(&tempPayload)

	if decodeErr == nil {
		_, replaceError := collection.ReplaceOne(context.TODO(), bson.M{"_id": id}, payload)
		return replaceError
	}
	_, insertError := collection.InsertOne(context.TODO(), payload)
	return insertError
}

func (m *MongoConnection) UpdateUserInfo(collectionString string, key string, value string, payload interface{}) error {
	var collection = m.Database.Collection(collectionString)
	tempPayload := payload

	item := collection.FindOne(context.TODO(), bson.M{key: value})

	_ = item.Decode(&tempPayload)
	if tempPayload == nil {

	}
	_, updateErr := collection.ReplaceOne(context.TODO(), bson.M{key: value}, payload)
	return updateErr
}

func getNewMongoClient(connectionString string) (*MongoConnection, error) {
	c, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(c, options.Client().ApplyURI(connectionString))
	defer cancel()
	if err != nil {
		return nil, err
	}

	mongoConnection := MongoConnection{
		Client: client,
	}

	return &mongoConnection, nil
}

//VerifyConnection returns error if it cannot connect to the database
func (m *MongoConnection) VerifyConnection() error {
	c, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	err := m.Client.Ping(c, nil)
	defer cancel()
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}
