package database

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

//MongoWrapper wraps mongo.Client so methods can be added and tester
type MongoWrapper interface {
	GetCollection(string, string, string, interface{}) error
	GetCollectionByID(string, primitive.ObjectID, interface{}) error
	InsertCollection(string, string, string, interface{}) (string, error)
	UpsertCollectionByID(string, primitive.ObjectID, interface{}) error
	GetAllCollectionsByForeignKey(string, string, primitive.ObjectID, interface{}) error
	GetAll(collectionString string) (*mongo.Cursor, error)
	GetRandom(string, interface{}) error
	VerifyConnection() error
	UpdateUserInfo(string, string, string, interface{}) error
}
