package analytics

import (
	"bytes"
	"capstone/backend/internal/models"
	"capstone/backend/internal/server"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MongoMock struct {
	mock.Mock
}

func (m *MongoMock) GetCollection(col string, key string, value string, payload interface{}) error {
	user := models.User{
		ID: primitive.NewObjectID().String(),
	}

	payload = user

	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetCollectionByID(s string, id primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) InsertCollection(col string, key string, value string, payload interface{}) (string, error) {
	args := m.Called()
	return "", args.Error(0)
}

func (m *MongoMock) UpsertCollection(s string) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAllCollectionsByForeignKey(collectionString string, key string, value primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) GetAll(collectionString string) (*mongo.Cursor, error) {
	args := m.Called()
	return nil, args.Error(0)
}

func (m *MongoMock) GetRandom(collectionString string, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpsertCollectionByID(collectionString string, id primitive.ObjectID, payload interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) VerifyConnection() error {
	args := m.Called()
	return args.Error(0)
}

func (m *MongoMock) UpdateUserInfo(a string, b string, c string, d interface{}) error {
	args := m.Called()
	return args.Error(0)
}

func performRequest(r http.Handler, method, path string, body []byte) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, bytes.NewBuffer(body))
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestTopSongsGivenEmptyUserIDReturnsStatusCode400(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(errors.New("generic error"))

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAnalyticEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/topsongs", nil)

	assert.Equal(t, http.StatusBadRequest, response.Code)
}

func TestTopSongsGivenBadUserIDReturnsStatusCode400(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(errors.New("generic error"))

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAnalyticEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/topsongs?id=BAD", nil)

	assert.Equal(t, http.StatusBadRequest, response.Code)
}

func TestTopSongsGivenDbGetFailsReturnsStatusCode500(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollectionByID").Return(errors.New("generic error"))

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAnalyticEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/topsongs?id=cas6193", nil)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
}

func TestTopSongsGivenDbGetSucceedsReturnsStatusCode200(t *testing.T) {
	mockDb := new(MongoMock)

	mockDb.On("GetCollection").Return(nil)

	mockDb.On("GetCollectionByID").Return(nil)

	testEngine := gin.Default()

	testServer := &server.Server{
		Db:     mockDb,
		Router: testEngine,
	}

	SetupAnalyticEndpoint(testServer)

	response := performRequest(testEngine, "GET", "/topsongs?id=5e951670daa60948cbcdc432", nil)

	assert.Equal(t, http.StatusOK, response.Code)
}
