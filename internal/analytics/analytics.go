package analytics

import (
	"capstone/backend/internal/models"
	"capstone/backend/internal/server"
	"fmt"
	"net/http"
	"sort"
	"strings"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func SetupAnalyticEndpoint(s *server.Server) {
	s.Router.Handle("GET", "/topsongs", handleUserTop(s))
}

func handleUserTop(s *server.Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		var userPlayData models.UserPlayData

		var user models.User

		userId := c.Query("id")

		getUserErr := s.Db.GetCollection("userData", "username", userId, &user)

		var userIDHex = strings.Replace(user.ID, "ObjectID(\"", "", 1)
		userIDHex = strings.Replace(userIDHex, "\")", "", 1)

		fmt.Println(userIDHex)

		if getUserErr != nil {
			fmt.Println(getUserErr)
			c.AbortWithStatusJSON(400, gin.H{"error": "invalid userID"})
		}

		userID, err := primitive.ObjectIDFromHex(userIDHex)

		if err != nil {
			fmt.Println(err)
			c.AbortWithStatusJSON(400, gin.H{"error": "invalid userID"})
			return
		}

		getErr := s.Db.GetCollectionByID("userData", userID, &userPlayData)

		if getErr != nil {
			c.AbortWithStatusJSON(500, gin.H{"error": "internal server error"})
			return
		}

		sort.Slice(userPlayData.PlayData[:], func(i, j int) bool {
			return userPlayData.PlayData[i].Count > userPlayData.PlayData[j].Count
		})

		c.JSON(http.StatusOK, userPlayData)
	}
}
