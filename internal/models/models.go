package models

import "go.mongodb.org/mongo-driver/bson/primitive"

//User object for user data and music library
type User struct {
	ID       string             `json:"_id" bson:"_id,omitempty"`
	Username string             `json:"username" bson:"username"`
	Password string             `json:"password" bson:"password"`
	Email    string             `json:"email" bson:"email"`
	Library  primitive.ObjectID `json:"library" bson:"library"`
}

//UserLibrary for user specific music
type UserLibrary struct {
	ID      primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Content string             `json:"content" bson:"content"`
}

//Artist contains artist name and album list
type Artist struct {
	ID   primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name string             `json:"name" bson:"name"`
	Path string             `json:"path" bson:"path"`
}

//Album contains album metadata and song list
type Album struct {
	ID       primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Title    string             `json:"title" bson:"title"`
	Artist   primitive.ObjectID `json:"artist" bson:"artist"`
	AlbumArt string             `json:"album_art" bson:"album_art"`
	Path     string             `json:"path" bson:"path"`
}

//Song contains song metadata
type Song struct {
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Title     string             `json:"title" bson:"title"`
	ArtistID  primitive.ObjectID `json:"artist_id" bson:"artist_id"`
	AlbumID   primitive.ObjectID `json:"album_id" bson:"album_id"`
	Path      string             `json:"path" bson:"path"`
	PlayCount int                `json:"playcount" bson:"playcount"`
}

//Playlist stores name and song list
type Playlist struct {
	ID      primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	UserID  primitive.ObjectID `json:"user_id" bson:"user_id"`
	Content string             `json:"content" bson:"content"`
}

//DTO to return a list of all user playlists
type UserPlaylists struct {
	IDs []primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
}

//MusicVideo contains associated song and  metadata
type MusicVideo struct {
	ID             primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	AssociatedSong primitive.ObjectID `json:"associated_song" bson:"associated_song"`
	//StreamURL string
}

type UserPlayData struct {
	ID       primitive.ObjectID `json:"_id" bson:"_id"`
	PlayData []SongPlayData     `json:"songs" bson:"songs"`
}

type SongPlayData struct {
	ID     primitive.ObjectID `json:"_id" bson:"_id"`
	Title  string             `json:"title" bson:"title"`
	Artist string             `json:"artist" bson:"artist"`
	Album  string             `json:"album" bson:"album"`
	Count  int                `json:"count" bson:"count"`
}

type SongReccomendation struct {
	ID               primitive.ObjectID `json:"_id" bson:"_id"`
	ReccomendedSongs map[string]float32 `json:"reccomendations" bson:"reccomendations"`
}

type ReccomendedPlaylist struct {
	Title string         `json:"title" bson:"title"`
	Songs []PlaylistSong `json:"songs" bson:"songs"`
}

type PlaylistSong struct {
	Title            string `json:"title" bson:"title"`
	StreamURL        string `json:"streamURL" bson:"streamURL"`
	ArtistName       string `json:"artistName" bson:"artistName"`
	AlbumTitle       string `json:"albumTitle" bson:"albumTitle"`
	AlbumOrderNumber string `json:"albumOrderNumber" bson:"albumOrderNumber"`
	DisplayName      string `json:"displayName" bson:"displayName"`
}

type UserLibraryContent struct {
	LibrarySongs []PlaylistSong        `json:"songs" bson:"songs"`
	Playlists    []ReccomendedPlaylist `json:"playlists" bson:"playlists"`
}
