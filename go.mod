module capstone/backend

go 1.13

require (
	github.com/gin-gonic/gin v1.6.2
	github.com/stretchr/testify v1.4.0
	go.mongodb.org/mongo-driver v1.3.2
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71
)
