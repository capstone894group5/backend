package main

import (
	"capstone/backend/internal/analytics"
	"capstone/backend/internal/auth"
	"capstone/backend/internal/database"
	"capstone/backend/internal/healthcheck"
	"capstone/backend/internal/library"
	"capstone/backend/internal/music"
	"capstone/backend/internal/player"
	"capstone/backend/internal/playlist"
	"capstone/backend/internal/reccomendation"
	"capstone/backend/internal/server"
	"fmt"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	//Allow errors to be returned
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}

func run() error {
	//Set up dependencies
	db, err := database.SetupDatabase()
	if err != nil {
		return err
	}
	router := gin.Default()
	//router.StaticFile("/login", "../api/public/index.html")
	router.Use(server.CORSMiddleware())
	//Create server with dependencies
	srv := &server.Server{
		Db:     db,
		Router: router,
	}
	//Setup routing and static files
	//srv.routes()

	reccomendation.SongReccomendations(srv)
	reccomendation.CreateReccomenededPlaylists(srv)

	music.SyncSongDatabase("cmd/api/music/", srv)

	analytics.SetupAnalyticEndpoint(srv)
	player.SetupPlayerEndpoint(srv)
	auth.SetupAuthEndpoint(srv)
	healthcheck.SetupHealthcheckEndpoint(srv)
	playlist.SetupPlaylistEndpoint(srv)
	library.SetupLibraryEndpoint(srv)

	// srv.Router.LoadHTMLGlob("../api/public/*")
	// srv.Router.LoadHTMLFiles("../../internal/player/player.html")

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	srv.Router.Run(":" + port)
	//srv.Router.RunTLS(":8080", "./cert.pem", "./key.pem") //Listen and serve on "http://localhost:8080"
	return nil
}
